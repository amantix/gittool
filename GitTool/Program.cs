﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitTool
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!File.Exists("repositories.txt")) return;

            using (var sw = new StreamReader("repositories.txt"))
            {
                var startInfo = new ProcessStartInfo("git.exe")
                {
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                };

                var process = new Process {StartInfo = startInfo};

                //process.StartInfo.Arguments =
                    //"config --global --unset http.proxy"; // Отключить прокси
                //"config --global http.proxy http://user:password@proxy.int.kpfu.ru:8080"; // Установить прокси
                process.Start();
                process.WaitForExit();

                while (!sw.EndOfStream)
                {
                    var url = sw.ReadLine();
                    var dir = Path.GetFileNameWithoutExtension(url);
                    Debug.Assert(dir != null, "dir != null");
                    Directory.CreateDirectory(dir);

                    Console.WriteLine(dir);

                    process.StartInfo.WorkingDirectory = dir;

                    process.StartInfo.Arguments = "init";
                    process.Start();
                    process.WaitForExit();

                    process.StartInfo.Arguments = $"remote add origin {url}";
                    process.Start();
                    process.WaitForExit();

                    process.StartInfo.Arguments = "pull origin master";
                    process.Start();
                    process.WaitForExit();
                }
            }
        }
    }
}
